from rest_framework.routers import DefaultRouter
from .views import PostViewSet, UserViewSet, LikesAnalyticViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'posts', PostViewSet)
router.register(r'analytics', LikesAnalyticViewSet, basename='analytics')

urlpatterns = router.urls

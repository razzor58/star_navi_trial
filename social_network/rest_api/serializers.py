from django.contrib.auth.models import User
from rest_framework import serializers
from . import models


class LikesAnalyticSerializer(serializers.Serializer):
    created_at = serializers.DateField()
    total_likes = serializers.IntegerField()


class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = models.Post
        fields = ['id', 'title', 'content', 'author', 'likes']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    last_login = serializers.DateTimeField(read_only=True)
    last_action_time = serializers.DateTimeField(source='profile.last_action_time', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'last_login', 'last_action_time']

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

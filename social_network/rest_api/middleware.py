from datetime import datetime
from django.contrib.auth.models import AnonymousUser
from .models import UserProfile


class LogRequestMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if not isinstance(request.user, AnonymousUser):
            user_profile = UserProfile.objects.filter(user=request.user).first()
            if user_profile:
                user_profile.last_action_time = datetime.now()
                user_profile.save()

        return response

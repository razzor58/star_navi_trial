from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth.models import User
from django.db.models import Count
from .models import Post, Like
from .serializers import PostSerializer, UserSerializer, LikesAnalyticSerializer


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_permissions(self):
        if self.action == 'create':
            return [AllowAny(), ]
        return super().get_permissions()


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def perform_create(self, serializer):
        return serializer.save(author=self.request.user)

    @action(detail=True, methods=['post'])
    def like(self, request, pk=None):
        post = self.get_object()
        post.likes += 1
        post.save()
        _ = Like.objects.create(post=post)
        return Response({'status': 'liked'})

    @action(detail=True, methods=['post'])
    def unlike(self, request, pk=None):
        post = self.get_object()
        if post.likes > 0:
            post.likes -= 1
            post.save()
        return Response({'status': 'unliked'})


class LikesAnalyticViewSet(viewsets.ModelViewSet):
    serializer_class = LikesAnalyticSerializer

    def get_queryset(self):
        date_from = self.request.GET.get('date_from', '2000-01-01')
        date_to = self.request.GET.get('date_to', '2100-01-01')
        return (Like.objects
                .values('created_at')
                .filter(created_at__gt=date_from, created_at__lt=date_to)
                .annotate(total_likes=Count('id')))

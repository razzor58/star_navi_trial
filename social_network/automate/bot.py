from random import randint, choice
from string import ascii_lowercase
from requests import Request, Session

from .config import (NUMBER_OF_USERS, MAX_POSTS_PER_USER, MAX_LIKES_PER_USER,
                     BASE_URL,  DEFAULT_PASSWORD,  MAX_POST_CONTENT_LEN)


class ApiClient:
    def __init__(self):
        self.token = None

    def _request(self, method, path, **kwargs):
        req = dict()
        req['url'] = f'{BASE_URL}/{path}'
        req['params'] = kwargs.get('params')
        req['json'] = kwargs.get('json')

        if not kwargs.get('without_auth'):
            req['headers'] = {'Authorization': f'JWT {self.token}'}

        req_for_send = Request(method, **req).prepare()
        session = Session()
        raw_response = session.send(req_for_send)

        return raw_response.json()

    def sign_up(self, user_name):
        return self._request('post', 'api/v1/users/', json={
            'username': user_name,
            'email': f'{user_name}_address@mail.com',
            'password': DEFAULT_PASSWORD
        }, without_auth=True)

    def login(self, user_name):
        self.token = self._request('post', 'rest-auth/login/',
                                   json={'username': user_name, 'password': DEFAULT_PASSWORD},
                                   without_auth=True).get('token')

    def create_post(self, post_num):
        return self._request('post', 'api/v1/posts/',
                             json={
                                 'title': f'Post #{post_num}',
                                 'content': ''.join(choice(ascii_lowercase) for _ in range(MAX_POST_CONTENT_LEN))
                             })

    def get_posts(self):
        return self._request('get', 'api/v1/posts')

    def get_users(self):
        return self._request('get', 'api/v1/users/', without_auth=True)

    def like_post(self, post_id):
        return self._request('post', f'api/v1/posts/{post_id}/like/')

    def get_analytics(self, **kwargs):
        return self._request('get', 'api/v1/analytics',
                             params={
                                 'date_from': kwargs.get('date_from', '2020-01-01'),
                                 'date_to': kwargs.get('date_to', '2021-01-01'),
                             })


class ApiUserBot:
    def __init__(self):

        self.api = ApiClient()
        self.user_name = self._get_user_name()

        self.api.sign_up(user_name=self.user_name)
        self.api.login(user_name=self.user_name)

    def __repr__(self):
        return f'ApiBot({self.user_name})'

    def _get_user_name(self):
        """ Generate unique user name """
        users_ids = [user['id'] for user in self.api.get_users()]
        user_next_number = max(users_ids) + 1 if users_ids else 0
        return f'user_{user_next_number}'

    def _create_posts(self):
        """ Create  multiple post up to MAX_POSTS_PER_USER setting """

        for post_num in range(randint(0, MAX_POSTS_PER_USER)):
            self.api.create_post(post_num)

    def _do_likes(self):
        """ Perform multiple likes up to MAX_LIKES_PER_USER setting """

        all_posts_ids = [item['id'] for item in self.api.get_posts()]

        for _ in range(randint(0, MAX_LIKES_PER_USER)):
            post_id = choice(all_posts_ids)
            self.api.like_post(post_id)

    def _show_stats(self):
        """ Return report data """
        return self.api.get_analytics()

    def do_trial_case(self):
        """ Registered NUMBER_OF_USERS users and perform posts and likes """
        self._create_posts()
        self._do_likes()

        return self._show_stats()


def start_api_trial():
    stats = 'Trial is not executed'

    for _ in range(NUMBER_OF_USERS):
        bot = ApiUserBot()
        stats = bot.do_trial_case()

    print(stats)

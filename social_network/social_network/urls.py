from django.urls import path, include

urlpatterns = [
    path('api/v1/', include('rest_api.urls')),
    path('rest-auth/', include('rest_auth.urls')),
]

#### Social Network (demo project)
REST API build by Django Rest Framework according to [trial task descirption](task.md)

#### HOW-TO
1. Clone repository `git clone https://gitlab.com/razzor58/star_navi_trial.git`

1. Go to  project root directory `cd star_navi_trial`

1. Check config file `social_network/automate/config.py` and change it, if you want

1. Build image and run app container
    ```sh
    $ docker-compose up -d --build
    ```
1. Check available methods via DRF GUI (post and put required authorization)

    http://127.0.0.1:8000/api/v1/
    

1. Run data population via `automated bot`
    ```sh
    $ docker exec -it star_navi_trial_web_1 python /usr/src/social_network/run.py
    ```
   
1. Check stat after script finished

    http://127.0.0.1:8000/api/v1/analytics/
    
1. Check objects details

    http://127.0.0.1:8000/api/v1/users/1/
    
    http://127.0.0.1:8000/api/v1/posts/1/

1. Shutdown running container
    ```sh
    $ docker-compose down
    ```
